﻿using System;
using System.Collections.Generic;

namespace lab3 {
    public class Character {
        public string Name { get; set; }
        public HealthPlayer Health { get; set; }
        public int Speed { get; set; }
        public Guid Id { get; set; }

        public Character(string name, int speed, int health) {
            Name = name;
            Health = new HealthPlayer(health);
            Speed = speed;
            Id = Guid.NewGuid();
        }

        // Перегрузка методу ToString() для зручного виводу даних про персонажа
        public override string ToString() {
            return $"Character: Name: {Name}, Speed: {Speed}, Health: {Health.Hp}, ID: {Id}";
        }
    }

    public class HealthPlayer {
        public int Hp { get; set; }

        public HealthPlayer(int health) {
            Hp = health;
        }

        // Перегрузка методу ToString() для зручного виводу даних про об'єкт класу HealthPlayer
        public override string ToString() {
            return $"HealthPlayer: Health: {Hp}";
        }
    }

    public class TestModel {
        public Dictionary<Guid, Character> CharactersDictionary { get; set; }
        public Dictionary<Guid, List<Character>> CategoryDictionary { get; set; }

        //створив ліст
        public List<Character> list = new List<Character>();

        public TestModel() {
            // Створюємо список персонажів
            List<Character> charactersList = new List<Character>()
            {
                new Character("Petya pravoslanyi", 100, 100),
                new Character("Skeleton", 10, 50),
                new Character("Gorbachov", 5, 200),
                new Character("Bobalo - boss", 50, 9999),
            };

            // присовїв 
            list = charactersList;

            // Заповнюємо словник персонажів, де ключ - це Id персонажа
            CharactersDictionary = new Dictionary<Guid, Character>();
            foreach (Character character in charactersList) {
                CharactersDictionary.Add(character.Id, character);
            }

            CategoryDictionary = new Dictionary<Guid, List<Character>>();

            List<Character> category1 = new List<Character>() { charactersList[0], charactersList[1] };
            List<Character> category2 = new List<Character>() { charactersList[2], charactersList[3] };

            CategoryDictionary.Add(Guid.NewGuid(), category1);
            CategoryDictionary.Add(Guid.NewGuid(), category2);
        }

    }
    public static class ExtensionMethods {
        public static string GetCharacterIDAsString(this Character character) {
            return character.Id.ToString();
        }

        public static void IncreaseHealth(this Character character, int amount) {
            character.Health.Hp += amount;
        }

        public static void DecreaseHealth(this Character character, int amount) {
            character.Health.Hp -= amount;
            if (character.Health.Hp < 0) {
                character.Health.Hp = 0;
            }
        }
    }
    public class CharacterNameComparer : IComparer<Character> {
        public int Compare(Character x, Character y) {
            if (x == null || y == null)
                return 0;

            // Сортування за іменем в алфавітному порядку
            return string.Compare(x.Name, y.Name);
        }
    }

    class Program {
        static void Main(string[] args) {
                TestModel testModel = new TestModel();

                // Виводимо дані про кожного персонажа
                Console.WriteLine("Characters:");



                foreach (var character in testModel.CharactersDictionary.Values)
                {
                    Console.WriteLine(character);
                }

                // Виводимо дані про категорії персонажів
                Console.WriteLine("\nCategories:");
                foreach (var category in testModel.CategoryDictionary)
                {
                    Console.WriteLine($"Category ID: {category.Key}");
                    Console.WriteLine("Characters:");
                    foreach (var character in category.Value)
                    {
                        Console.WriteLine(character);
                    }
                    Console.WriteLine();
                }
            }






        }
    }

