﻿using System.Reflection;

namespace lab3
{
    public class Tests
    {

        private TestModel testModel;


        [SetUp]
        public void Setup()
        {
            testModel = new TestModel();
        }


        //1
        [Test]
        public void Test1() {

            var r = testModel.list.Select(c => c.Health);

            Assert.That(r.ElementAt(2).Hp, Is.EqualTo(200));

        }

        //2
        [Test]
        public void Test2() {

            var result = testModel.CharactersDictionary.Values.Where(obj => obj.Health.Hp <= 100);
            Assert.That(result.Count(), Is.EqualTo(2));

        }
        [Test]
        public void Test3()
        {
            List<Character> charactersList = testModel.list;

            Character newCharacter = new Character("New Character", 20, 150);
            charactersList.Add(newCharacter);
            Assert.IsTrue(charactersList.Contains(newCharacter));

            Dictionary<Guid, Character> charactersDictionary = testModel.CharactersDictionary;

            Guid newCharacterId = Guid.NewGuid();
            Character newCharacter2 = new Character("New Character 2", 30, 200);
            charactersDictionary.Add(newCharacterId, newCharacter2);
            Assert.IsTrue(charactersDictionary.ContainsKey(newCharacterId));
        }
        //4
        [Test]
        public void Test4()
        {
            Character Misha = testModel.list[2];
            Misha.IncreaseHealth(1);
            Assert.That(Misha.Health.Hp, Is.EqualTo(201));
        }
        //5
        [Test]
        public void Test5()
        {
            var character = new
            {
                Name = "John",
                Speed = 100,
                Health = 150
            };

            Assert.That(character.Name, Is.EqualTo("John"));
        }

        [Test]
        //6
        public void Test6()
        {
            var comparer = new CharacterNameComparer();

            var sortedCharacters = testModel.list.OrderBy(c => c, comparer).ToList();
            Assert.That(sortedCharacters[0].Name, Is.EqualTo("Bobalo - boss"));
            Assert.That(sortedCharacters[3].Name, Is.EqualTo("Skeleton"));
            Assert.That(sortedCharacters[3].Speed, Is.EqualTo(10));

        }
        //7
        [Test]
        public void Test7()
        {
    Character[] characterArray = testModel.list.ToArray();
            Assert.That(characterArray.Length, Is.EqualTo(4));
            Assert.That(characterArray[0].Name, Is.EqualTo("Petya pravoslanyi"));
            Assert.That(characterArray[3].Health.Hp, Is.EqualTo(9999));
        }
        [Test]
        //8
        public void Test8()
        {
            Character[] charactersArray = testModel.list.ToArray();
            Array.Sort(charactersArray, (c1, c2) => c1.Health.Hp.CompareTo(c2.Health.Hp));
            Assert.That(charactersArray[0].Health.Hp, Is.EqualTo(50));
            Assert.That(charactersArray[charactersArray.Length - 1].Health.Hp, Is.EqualTo(9999));
        }

        //4
        [Test]
        public void TestDictionary()
        {
            Character character = testModel.CharactersDictionary[testModel.list[0].Id];
            Assert.AreEqual("Petya pravoslanyi", character.Name);
        }

        //6
        [Test]
        public void TestQueue()
        {
            var test = new Queue<Character>(testModel.list);
            Assert.That(test.Peek().Name, Is.EqualTo("Petya pravoslanyi"));
        }
        //7
        [Test]
        public void TestGroup()
        {
            var groupedCharacters = testModel.CharactersDictionary.Values
                .GroupBy(character => character.Health.Hp)
                .ToDictionary(group => group.Key, group => group.ToList());

            Assert.AreEqual(1, groupedCharacters[100].Count);
            Assert.AreEqual(1, groupedCharacters[50].Count);
            Assert.AreEqual(1, groupedCharacters[200].Count);
            Assert.AreEqual(1, groupedCharacters[9999].Count);

        }
        //8
        [Test]
        public void TestHarder()
        {
            var sortedCharacters = testModel.CharactersDictionary.Values.OrderBy(character => character.Name).ToList();
            Assert.AreEqual(4, sortedCharacters.Count);
        }


    }
}
