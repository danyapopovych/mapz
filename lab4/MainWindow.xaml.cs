﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Serialization;

namespace lab4 {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {
        public MainWindow() {
            InitializeComponent();
            Draw();
        }


        private NPCFactory fact = new NPCFactory();
        private ChoiceBuilder cBuilder= new ChoiceBuilder();
        private Choice choice;
        private INPC currentNPC;

        private void btn_create_npc_Click(object sender, RoutedEventArgs e) {

            NPCType type = ParseNPC(cb_npc.Text);
            currentNPC = fact.CreateNPC(type);

            
        }


        public NPCType ParseNPC(string npc) {
            switch (npc.ToLower()) {
                case "ment":
                    return NPCType.Ment;
                case "trollface":
                    return NPCType.Trollface;
                default:
                    throw new ArgumentException("Invalid room type string.", nameof(npc));
            }
        }

        private void btn_set_right_Click(object sender, RoutedEventArgs e) {
            cBuilder.SetRightChoice(currentNPC);
            
        }

        private void btn_set_top_Click(object sender, RoutedEventArgs e) {
            cBuilder.SetTopChoice(currentNPC);

        }

        private void btn_set_left_Click(object sender, RoutedEventArgs e) {
            cBuilder.SetLeftChoice(currentNPC);

        }

        private void btn_create_choice_Click(object sender, RoutedEventArgs e) {
            choice = cBuilder.Build();
            DrawChoice();

        }

        private void DrawChoice() {
            foreach (var choice in choice.choices) {
                Point p;

                if (choice.Key == Direction.Right) {
                    p = new Point(350, 130);
                } else if (choice.Key == Direction.Left) {
                    p = new Point(-10, 130);
                } else if (choice.Key == Direction.Top) {
                    p = new Point(150, -10);
                }
                string path = String.Empty;

                if (choice.Value.Name == "Ment Grisha") {
                    path = "C:\\Users\\Danylo\\Documents\\University\\4 term\\MAPZ\\mapz\\lab4\\ment.jpg";
                } else if (choice.Value.Name == "Trollface") {
                    path = "C:\\Users\\Danylo\\Documents\\University\\4 term\\MAPZ\\mapz\\lab4\\troll.png";
                }

                DrawImg(path, p);

            }
        }

       

  

        private void btn_interact_left_Click(object sender, RoutedEventArgs e) {
            string response = choice.MakeChoice(Direction.Left);
            lb_dialog.Content = response + "\n";
        }

        private void btn_interact_top_Click(object sender, RoutedEventArgs e) {
            string response = choice.MakeChoice(Direction.Top);
            lb_dialog.Content = response + "\n";

        }

        private void btn_interact_right_Click(object sender, RoutedEventArgs e) {
            string response = choice.MakeChoice(Direction.Right);
            lb_dialog.Content = response + "\n";
        }


        private void Draw() {
            ImageBrush brush = new ImageBrush();
            brush.ImageSource = new BitmapImage(new Uri("C:\\Users\\Danylo\\Documents\\University\\4 term\\MAPZ\\mapz\\lab4\\cross.png"));
            canvas.Background = brush;
        }


        public void DrawImg(string path, Point point) {
            BitmapImage bitmap = new BitmapImage(new Uri(path));
            Image image = new Image();
            image.Source = bitmap;
            image.Width = bitmap.Width / 2.8 ;
            image.Height = bitmap.Height / 2.8;
            Canvas.SetLeft(image, point.X);
            Canvas.SetTop(image, point.Y);
            canvas.Children.Add(image);
        }

    }
}
