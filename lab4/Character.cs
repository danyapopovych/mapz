﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab4 {
    public class Character {

        private static Character ch;
        private Character() {
            inteligence = -1;
        }

        public int inteligence;

        public static Character GetInstance() {
            if (ch == null) {
                ch = new Character();
            }
            return ch;
        }


        public void UpgradeInteligence(int iq) {
            inteligence += iq;
        }

    }
}
