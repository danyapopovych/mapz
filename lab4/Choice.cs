﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab4 {
    public enum Direction {
        Left,
        Right,
        Top
    }

    public class Choice {

        public Dictionary<Direction, INPC> choices = new Dictionary<Direction, INPC>();

        public void AddChoice(Direction dir, INPC npc) {
            choices[dir] = npc;
        }

        public override string ToString() {
            var sb = new StringBuilder();
            foreach (var choice in choices) {

                sb.AppendLine($"{choice.Key} {choice.Value.Name}");
            }
            return sb.ToString();
        }

        public string MakeChoice(Direction dir) {
            return choices[dir].Interact();
        }

    }

    public interface IChoiceBuilder {
        IChoiceBuilder SetRightChoice(INPC ch);
        IChoiceBuilder SetLeftChoice(INPC ch);
        IChoiceBuilder SetTopChoice(INPC ch);
        Choice Build();

    }


    public class ChoiceBuilder : IChoiceBuilder {
        private Choice choice = new Choice();


        public IChoiceBuilder SetLeftChoice(INPC ch) {
            this.choice.AddChoice(Direction.Left, ch);
            return this;
        }

        public IChoiceBuilder SetRightChoice(INPC ch) {
            this.choice.AddChoice(Direction.Right, ch);
            return this;


        }

        public IChoiceBuilder SetTopChoice(INPC ch) {
            this.choice.AddChoice(Direction.Top, ch);
            return this;

        }

        public Choice Build() {
            return choice;
        }
    }
}
