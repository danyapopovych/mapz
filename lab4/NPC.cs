﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab4 {
    public interface INPC {
        string Name { get; }
        string Interact();
    }

    public class Trollface : INPC {
        public string Name => $"Trollface Daun";

        public string Interact() {
            return $"{Name}: Hello adventurer!!!";
        }
    }

    public class Ment : INPC {
        public string Name => $"Ment Grisha";

        public string Interact() {
            return $"{Name}: Hello adventurer!!!";
        }
    }

    public interface IFactory {
        INPC CreateNPC();
    }

    public class TrollfaceFactory : IFactory {
        public INPC CreateNPC() {
            Console.WriteLine("TrollFace was created");
            return new Trollface();
        }
    }

    public class MentFactory : IFactory {
        public INPC CreateNPC() {
            Console.WriteLine("Ment was created");
            return new Ment();
        }
    }



    public enum NPCType {
        Trollface,
        Ment
    }

    public class NPCFactory {
        private Dictionary<NPCType, IFactory> f = new Dictionary<NPCType, IFactory>();

        public NPCFactory() {
            f[NPCType.Trollface] = new TrollfaceFactory();
            f[NPCType.Ment] = new MentFactory();
        }

        public INPC CreateNPC(NPCType type) {
            return f[type].CreateNPC();
        }

    }
}
