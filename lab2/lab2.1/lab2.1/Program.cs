﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using BenchmarkDotNet.Running;

namespace lab2
{
    class BaseClass
    {
        private string baseField;

        // Перевантажений конструктор базового класу
        public BaseClass(string baseField)
        {
            this.baseField = baseField;
        }
    }

    // Похідний клас
    class DerivedClass : BaseClass
    {
        private string derivedField;

        // Перевантажений конструктор похідного класу
        public DerivedClass(string baseField, string derivedField) : base(baseField) // Виклик конструктора базового класу
        {
            this.derivedField = derivedField;
        }

        // Інший конструктор похідного класу, який викликає конструктор поточного класу
        public DerivedClass(string derivedField) : this("DefaultBaseValue", derivedField) // Виклик іншого конструктора поточного класу
        {
        }
    }


    // abstract class
    abstract class Footballer
    {
        public string position;
        public string team;
        private int age;

        protected Footballer(string position, string team)
        {
            this.position = position;
            this.team = team;
        }

        public abstract void Play();

        public override string ToString()
        {
            return $"Position: {position}, Team: {team}";
        }

        // override Equals
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }
            Footballer other = (Footballer)obj;
            return position == other.position && team == other.team;
        }

        // override GetHashCode()
        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 17;
                hash = hash * 23 + position.GetHashCode();
                hash = hash * 23 + team.GetHashCode();
                return hash;
            }
        }
    }

    //interface
    interface IFootballer
    {
        void SignContract();
    }



    // basic class
    internal class Attacker : Footballer, IFootballer // mnozhynne
    {

        private class Forward { };
        // private is less accessible
        //public Forward Messi;

        private bool isMainPlayer;
        private string flang;

        // yavnyi explicit
        public static explicit operator string(Attacker attacker)
        {
            return $"Attacker: {attacker.position}, {attacker.team}";
        }

        // neyavnyi implicit
        public static implicit operator Attacker(string data)
        {
            string[] parts = data.Split(',');
            if (parts.Length != 2)
            {
                throw new ArgumentException("Invalid string format");
            }

            string position = parts[0].Trim();
            string team = parts[1].Trim();

            return new Attacker(true, "Left", position, team);
        }

        public Attacker(bool isMainPlayer, string flang, string position, string team) : base(position, team) // base
        {
            this.isMainPlayer = isMainPlayer;
            this.flang = flang;
        }

        void IFootballer.SignContract()
        {
            Console.WriteLine("I signed new contact");
        }

        public override void Play()
        {
            Console.WriteLine("I play for the best club");
        }
    }

    enum Goals // enum perelichuvanyi typ
    {
        Haaland = 1,
        BernardoSilva = 2,
        DeBruyne = 3,
        Ederson = 4,
        Alvarez = BernardoSilva ^ Haaland, //viddilyaemo bity 2 vid 1 
        Doku = DeBruyne | Haaland | BernardoSilva, // pobitove abo, pobitove dodavania
        Kompany = ~Ederson, //invertuvania
        Foden = Haaland & BernardoSilva, // pobitove i, pobitove mnozhenya

    }
    class Initialization
    {
        private int dynamicField = 1;
        protected static int staticField = 10;

        public Initialization()
        {
            Console.WriteLine("Dynamic constructor");
        }

        static Initialization()
        {
            ++staticField;
            Console.WriteLine("Static constructor");
        }
    }

    class InitializationChild : Initialization
    {
        public InitializationChild()
        {
            Console.WriteLine("Dynamic child constructor");
        }

        static InitializationChild()
        {
            ++staticField;
            Console.WriteLine("Static child constructor");
        }
    }





    class Manager // class, heart, internal
    {
        Manager() { } // constructor, lock, private
        string club; // pole, lock, private
        void Managing() { } // method, lock, private
        class Tactic { } // vklad class, lock, private

    }
    interface IManager // , interface, heart, internal
    {
        void SignContract(); // method, nothing, public
    }
    struct Club //  struct, heart, internal
    {
        int count; // pole, lock, private
    }


    // nasliduvaniya struct by inteerface
    /*struct Tactic: IFootballer
    {
        string style;

        public void SignContract() {
            Console.WriteLine("Sign to play by plan");
        }
    }
    abstract struct Footballer1
    {
        public string position;
        public string team;
        private int age;

        protected Footballer1(string position, string team)
        {
            this.position = position;
            this.team = team;
        }

        public abstract void Play();
    }

    //interface
    interface IFootballer1
    {
        void SignContract();
    }


    // basic class
    internal struct Attacker1 : Footballer1, IFootballer1 
    {

        private bool isMainPlayer;
        private string flang;


        public Attacker(bool isMainPlayer, string flang, string position, string team) : base(position, team) // base
        {
            this.isMainPlayer = isMainPlayer;
            this.flang = flang;
        }


        public override void Play()
        {
            Console.WriteLine("I play for the best club");
        }
    }*/











    public class Program
    {
        // out
        public static void GetValueOut(out int value)
        {
            value = 10;
        }

        // ref
        public static void GetValueRef(ref int value)
        {
            value = value * 2;
        }
        public static void Main(string[] args)
        {
            var summary = BenchmarkRunner.Run<MethodsBenchmark>();
            // internal
            //Attacker Haaland = new Attacker(true, "Left", "Forward", "FC Mancester City");

            // public
            //Console.WriteLine("Attacker position: " + Haaland.position);

            // protected
            //Console.WriteLine("Attacker team: " + Haaland.team); 

            //private
            //Console.WriteLine("Attacker age: " + Haaland.age);
            int a = 5;
            int b;
            GetValueOut(out b);
            Console.WriteLine("Value after out: " + b);

            GetValueRef(ref a);
            Console.WriteLine("Value after ref: " + a);

            // ref bez inicializacii
            // c;
            //GetValueRef(ref c); //ref need initialiaztion inside function




            int d;
            GetValueOut(out d); // out need initialization in main


            int intValue = 10;
            object boxedValue = intValue; //boxing,  int -> object

            Console.WriteLine("Boxed value: " + boxedValue);


            int unboxedValue = (int)boxedValue; // unboxing, object -> int

            Console.WriteLine("Unboxed value: " + unboxedValue);


            // internal
            Attacker Haaland = new Attacker(true, "Left", "Forward", "FC Manchester City");

            // public
            Console.WriteLine("Attacker position: " + Haaland.position);

            string attackerString = (string)Haaland; // yavne
            Console.WriteLine("Attacker info (explicit conversion): " + attackerString);

            string data = "Midfielder, Manchester United";
            Attacker newAttacker = data; // neyavne
            Console.WriteLine("New attacker info (implicit conversion): " + newAttacker.position + ", " + newAttacker.team);
        }
        // Створення об'єкта похідного класу з використанням перевантаженого конструктора
        DerivedClass obj1 = new DerivedClass("BaseValue", "DerivedValue");

        // Створення об'єкта похідного класу з використанням іншого перевантаженого конструктора
        DerivedClass obj2 = new DerivedClass("DerivedValue");


    }

}

