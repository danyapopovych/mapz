﻿using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Running;

[MemoryDiagnoser]
public class MethodsBenchmark
{
    private const int iterations = 1000;

    [Benchmark]
    public void TestPrivateMethod()
    {
        MethodsTest obj = new MethodsTest();
        for (int i = 0; i < iterations; i++)
        {
            obj.CallPrivateMethod();
        }
    }

    [Benchmark]
    public void TestProtectedMethod()
    {
        MethodsTest obj = new MethodsTest();
        for (int i = 0; i < iterations; i++)
        {
            obj.CallProtectedMethod();
        }
    }

    [Benchmark]
    public void TestPublicMethod()
    {
        MethodsTest obj = new MethodsTest();
        for (int i = 0; i < iterations; i++)
        {
            obj.CallPublicMethod();
        }
    }

    [Benchmark]
    public void TestNestedPrivateMethod()
    {
        MethodsTest.NestedClass obj = new MethodsTest.NestedClass();
        for (int i = 0; i < iterations; i++)
        {
            obj.CallNestedPrivateMethod();
        }
    }

    [Benchmark]
    public void TestNestedProtectedMethod()
    {
        MethodsTest.NestedClass obj = new MethodsTest.NestedClass();
        for (int i = 0; i < iterations; i++)
        {
            obj.CallNestedProtectedMethod();
        }
    }

    [Benchmark]
    public void TestNestedPublicMethod()
    {
        MethodsTest.NestedClass obj = new MethodsTest.NestedClass();
        for (int i = 0; i < iterations; i++)
        {
            obj.CallNestedPublicMethod();
        }
    }
}


public class MethodsTest
{
    int privateCounter = 0;
    int protectedCounter = 0;
    int publicCounter = 0;

    private void PrivateMethod()
    {
        privateCounter++;
    }
    protected void ProtectedMethod()
    {
        protectedCounter++;
    }
    public void PublicMethod()
    {
        publicCounter++;
    }

    public void CallPrivateMethod()
    {
        PrivateMethod();
    }
    public void CallProtectedMethod()
    {
        ProtectedMethod();
    }
    public void CallPublicMethod()
    {
        PublicMethod();
    }

    public class NestedClass
    {
        int privateCounter = 0;
        int protectedCounter = 0;
        int publicCounter = 0;

        private void NestedPrivateMethod()
        {
            privateCounter++;
        }
        protected void NestedProtectedMethod()
        {
            protectedCounter++;
        }
        public void NestedPublicMethod()
        {
            publicCounter++;
        }

        public void CallNestedPrivateMethod()
        {
            NestedPrivateMethod();
        }
        public void CallNestedProtectedMethod()
        {
            NestedProtectedMethod();
        }
        public void CallNestedPublicMethod()
        {
            NestedPublicMethod();
        }

    }

}
